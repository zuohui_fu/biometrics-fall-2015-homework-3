filepath='genuine_impostor_pairs.txt'
fileid=fopen(filepath);
template1=zeros(20,480);
template2=zeros(20,480);
mask1=zeros(20,480);
mask2=zeros(20,480);
hd=zeros(1,550);
scales=1;

for i=1:550
   tline=fgets(fileid);
   file1=tline(1:20);
   file2=tline(22:41);
   for j=1:100
       if isequal(image_name{j},file1);
           template1=image_mat(:,:,j);
           mask1=image_mask(:,:,j);
       end
       
   end
   for k=1:100        
        if isequal(image_name{k},file2);
           template2=image_mat(:,:,k);
           mask2=image_mask(:,:,k);
        end
       
   end
   hd(i) = gethammingdistance(template1, mask1, template2, mask2, scales);
end
 X1=hd(1:50);
 X2=hd(51:550);
 X1_s=sort(X1);
 X2_s=sort(X2);

 mean1=mean(X1_s)
 mean2=mean(X2_s)
 std1=std(X1_s)
 std2=std(X2_s)


X_g_1=normpdf(X1_s,mean1,std1);
X_g_2=normpdf(X2_s,mean2,std2);

[n1,xout1]=hist(X_g_1);
[n2,xout2]=hist(X_g_2);
plot(X2_s, X_g_2, X1_s, X_g_1)
temp=X1(1)
for i=1:50
if temp>X1(i);
   temp=X1(i);
   j1=i
end
end
temp=X2(1)
% for i=1:50
% if temp<X1(i);
%    temp=X1(i);
%    j1=i
% end
% end
% temp=X2(1)
for i=51:500
if temp>X2(i);
   temp=X2(i);
   j2=i
end
% for i=51:500
% if temp<X2(i);
%    temp=X2(i);
%    j2=i
% end
end