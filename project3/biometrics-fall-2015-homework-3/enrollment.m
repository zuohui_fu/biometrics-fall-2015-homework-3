current_path= pwd;
raw_image_path=strcat(current_path,'/iris_dataset/')
%fprintf(raw_image_path)
file = dir(fullfile(raw_image_path,'*.tiff'));
totimages = numel(file);
image_mat=zeros(20, 480, totimages);
image_mask=zeros(20, 480, totimages);
image_name=cell(totimages,1); 
 for i = 1: totimages
     name= fullfile(raw_image_path, file(i).name);
     name_=cellstr(file(i).name)
     [template, mask] = createiristemplate(name);
     image_mat(:,:,i)=template;
     image_mask(:,:,i)=mask;
     image_name(i,1)=name_;
 end
 save('enrollraw.mat')